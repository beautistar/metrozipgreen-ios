//
//  AppDelegate.h
//  MetroZipGreen
//
//  Created by Astolfo Arcuri on 15/07/16.
//  Copyright © 2016 Beauti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "MBProgressHUD.h"
#import "UserEntity.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    
    MBProgressHUD *HUD;
    UserEntity *Me;
}

@property (nonatomic, strong) UserEntity * Me;

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

// show loading state when we connect to server
- (void) showLoadingViewWithTitle:(NSString *) title sender:(id) sender;

// hide loading view
-(void) hideLoadingView;
- (void) hideLoadingView : (NSTimeInterval) delay;

// show alert dialog
- (void) showAlertDialog : (NSString *)title message:(NSString *) message positive:(NSString *)strPositivie negative:(NSString *) strNegative sender:(id) sender;



@end

