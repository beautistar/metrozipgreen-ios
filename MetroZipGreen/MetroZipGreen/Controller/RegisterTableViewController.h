//
//  RegisterTableViewController.h
//  MetroZipGreen
//
//  Created by Astolfo Arcuri on 16/07/16.
//  Copyright © 2016 Beauti. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterTableViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UIView *vForTermCondition;
@property (weak, nonatomic) IBOutlet UITextView *txvAddress;



@end
