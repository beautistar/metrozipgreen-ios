//
//  CompletedTripViewController.m
//  MetroZipGreen
//
//  Created by Astolfo Arcuri on 17/07/16.
//  Copyright © 2016 Beauti. All rights reserved.
//

#import "CompletedTripViewController.h"
#import "TripCell.h"

@interface CompletedTripViewController () <UITableViewDelegate, UITableViewDataSource>

@end

@implementation CompletedTripViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableview delegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 20;
}



- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 100;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * cellIdentifier =   @"TripCell";
    
    // return dictionary cell
    
    TripCell * cell = (TripCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    [cell setItem];
    
    return cell;
}


@end
