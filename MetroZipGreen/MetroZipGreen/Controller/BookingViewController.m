//
//  BookingViewController.m
//  MetroZipGreen
//
//  Created by Astolfo Arcuri on 17/07/16.
//  Copyright © 2016 Beauti. All rights reserved.
//

#import "BookingViewController.h"
#import "TripCell.h"

@interface BookingViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation BookingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initView {
    
    // set sos image at the top right of title
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sos"]]];
    
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - tableview delegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 20;
}



- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 100;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * cellIdentifier =   @"TripCell";
    
    // return dictionary cell
    
    TripCell * cell = (TripCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    [cell setItem];
    
    return cell;
}




@end
