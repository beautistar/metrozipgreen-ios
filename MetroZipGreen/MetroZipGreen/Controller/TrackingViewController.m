//
//  TrackingViewController.m
//  MetroZipGreen
//
//  Created by Astolfo Arcuri on 17/07/16.
//  Copyright © 2016 Beauti. All rights reserved.
//

#import "TrackingViewController.h"

@interface TrackingViewController () {
    
    GMSMapView * mapView_;
    
    
//    CLLocationManager *locationManager;
//    CLLocation *myLoc;
//    
//    GMSMarker *_marker;
//    GMSMapView *_mapView;
//    GMSCameraPosition *camera;
//    
//    NSString *strCurLatitude, *strCurLongitude, *strForLatitude, *strForLongitude;

}

@end

@implementation TrackingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initView];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initView {
    
    // set logo image at the top center of title.
    UIImage *img = [UIImage imageNamed:@"title"];
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:img];
    
    // set sos image at the top right of title
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sos"]]];
    
    self.navigationItem.rightBarButtonItem = rightItem;
    
    // init google map
    
    // Create a GMSCameraPosition that tells the map to display the
    // coordinate -33.86,151.20 at zoom level 6.
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:28.6255569
                                                            longitude:77.1722878
                                                                 zoom:10];
    mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    mapView_.myLocationEnabled = YES;
    self.view = mapView_;
    
    // Creates a marker in the center of the map.
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(28.6255569, 77.1722878);
    marker.title = @"New Delhi";
    marker.snippet = @"India";
    marker.map = mapView_;
    
    [self.vForMap addSubview:mapView_];

}

- (IBAction)homeAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
