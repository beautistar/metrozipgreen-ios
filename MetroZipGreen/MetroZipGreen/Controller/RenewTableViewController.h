//
//  RenewTableViewController.h
//  MetroZipGreen
//
//  Created by Astolfo Arcuri on 17/07/16.
//  Copyright © 2016 Beauti. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RenewTableViewController : UITableViewController


@property (weak, nonatomic) IBOutlet UIView *vForDateBorder;

@end
