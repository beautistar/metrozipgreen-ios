//
//  AllTripViewController.m
//  MetroZipGreen
//
//  Created by Astolfo Arcuri on 17/07/16.
//  Copyright © 2016 Beauti. All rights reserved.
//

#import "AllTripViewController.h"
#import "TripCell.h"

@interface AllTripViewController () <UITableViewDelegate, UITableViewDataSource>

@end

@implementation AllTripViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    //NSLog(@"Here is all trip");
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 20;
}



- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 100;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * cellIdentifier =   @"TripCell";
    
    // return dictionary cell
    
    TripCell * cell = (TripCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    [cell setItem];
    
    return cell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
