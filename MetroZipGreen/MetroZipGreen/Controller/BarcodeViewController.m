//
//  BarcodeViewController.m
//  MetroZipGreen
//
//  Created by Astolfo Arcuri on 17/07/16.
//  Copyright © 2016 Beauti. All rights reserved.
//

#import "BarcodeViewController.h"

@interface BarcodeViewController ()

@end

@implementation BarcodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initView];
    
    [self setBarcode];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initView {
    
    // set logo image at the top center of title.
    UIImage *img = [UIImage imageNamed:@"title"];
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:img];
    
    // set sos image at the top right of title
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sos"]]];
    
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (void) setBarcode {
    
    self.vForBarcode.backgroundColor = [UIColor yellowColor];
    
//    float width;
//    int i;
    
//    NSLog(@"%f", self.vForBarcode.frame.size.width);

    
//    while (width < self.vForBarcode.frame.size.width) {
//        
//        i++;
//        
//        int dw = arc4random_uniform(5);
//        
//        NSLog(@"%d", dw);
//    
//        UIView *bar = [[UIView alloc] initWithFrame:CGRectMake(dw, self.vForBarcode.frame.origin.y, width, self.vForBarcode.frame.size.height)];
//        
//        if (i / 2 == 0) {
//            
//            bar.backgroundColor = [UIColor whiteColor];
//        } else {
//            bar.backgroundColor = [UIColor blackColor];
//        }
//        
//        [self.vForBarcode addSubview:bar];
//        
//        width = width + dw;
//        
//        
//        
//    }
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
