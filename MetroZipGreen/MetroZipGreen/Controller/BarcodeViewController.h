//
//  BarcodeViewController.h
//  MetroZipGreen
//
//  Created by Astolfo Arcuri on 17/07/16.
//  Copyright © 2016 Beauti. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BarcodeViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *vForBarcode;

@end
