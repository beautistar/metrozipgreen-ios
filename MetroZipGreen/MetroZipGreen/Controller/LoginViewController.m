//
//  LoginViewController.m
//  MetroZipGreen
//
//  Created by Astolfo Arcuri on 16/07/16.
//  Copyright © 2016 Beauti. All rights reserved.
//

#import "LoginViewController.h"
#import "CommonUtils.h"
#import "MainMenuViewController.h"

@interface LoginViewController () {
    
    UserEntity * _user;
}

@end

@implementation LoginViewController

@synthesize txfEmail, txfPassword;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //self.navigationController.navigationBarHidden = YES;
    
    [self initVars];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initVars {
    
    _user = APPDELEGATE.Me;
}

#pragma mark - login Action

- (IBAction)loginAction:(id)sender {
    
    if ([self isValid]) {
        
        [self doLogin];
        
    }
}

- (BOOL) isValid {
    
    if (txfEmail.text.length == 0) {
        
        [APPDELEGATE showAlertDialog:ALERT_TITLE message:INPUT_EMAIL positive:ALERT_OK negative:nil sender:self];
        
        return NO;
    } else if (txfPassword.text.length == 0) {
        
        [APPDELEGATE showAlertDialog:ALERT_TITLE message:INPUT_PWD positive:ALERT_OK negative:nil sender:self];
        
        return NO;
    }
    
    return YES;
}





- (void) doLogin {
    
    [APPDELEGATE showLoadingViewWithTitle:nil sender:self];
    
    NSString *email = [txfEmail.text encodeString:NSUTF8StringEncoding];
    NSString *password = [txfPassword.text encodeString:NSUTF8StringEncoding];
    
    NSDictionary *loginParams = @{
                             PARAM_USERNAME : email,
                             PARAM_PASSWORD : password,
                             PARAM_APPNAME : @"HIAAPP"
                             };
    
    NSDictionary *params = @{
                             PARAM_INTERFACE : @"RestAPI",
                             PARAM_METHOD : @"login",
                             PARAM_PARAMETERS : loginParams
                             };

    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_LOGIN];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    //requests.requestSerialize = [AFJSONSerialize serialize]
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"%@", responseObject);
        
        Boolean successful = [[responseObject valueForKey:RES_SUCCESSFUL] boolValue];
        
        if(successful) {
            
            NSLog(@"success");
            
//            if (photoPath.length != 0) {
//                
//                [self addNewEventPhoto];
//            }
//            
//            else {
//                
//                [APPDELEGATE hideLoadingView];
//                
//                [[JLToast makeText:NEW_EVENT_CREATED duration:2] show];
//                
//                [self gotoMyEventList];
            
            }
            
//        } else if (nResultCode == START_TIME_ERROR) {
//            
//            [APPDELEGATE hideLoadingView];
//            
//            [APPDELEGATE showAlertDialog:nil message:EVENT_TIME_ERROR positive:ALERT_OK negative:nil sender:self];
//            
//        } else if (nResultCode == END_TIME_ERROR) {
//            
//            [APPDELEGATE hideLoadingView];
//            
//            [APPDELEGATE showAlertDialog:nil message:EVENT_TIME_ERROR positive:ALERT_OK negative:nil sender:self];
//            
//        } else if (nResultCode == START_END_TIME_ERROR){
//            
//            [APPDELEGATE hideLoadingView];
//            
//            [APPDELEGATE showAlertDialog:nil message:EVENT_TIME_ERROR positive:ALERT_OK negative:nil sender:self];
//            
//        } else {
//            
//            [APPDELEGATE hideLoadingView];
//            
//            [APPDELEGATE showAlertDialog:nil message:PHOTO_UPLOAD_FAIL positive:ALERT_OK negative:nil sender:self];
//        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"error: %@", error);
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        
    }];
    

    
}

- (void) gotoMainVC {
    
    MainMenuViewController *mainVC = (MainMenuViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"MainMenuViewController"];
    
    [self.navigationController pushViewController:mainVC animated:YES];
    ;
    
    
    
    
}


/**********   for test
 
 
NSLog(@"%d", _event_id);
NSLog(@"%d", _user._idx);
NSLog(@"%@", txfTitle.text);
NSLog(@"%@", SelStartTime);
NSLog(@"%@", SelEndTime);
NSLog(@"%@", txfLocation.text);
NSLog(@"%@", txvDetail.text);

[APPDELEGATE showLoadingViewWithTitle:nil sender:self];
NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_ADD_NEWEVENT];


NSDictionary *params = @{
                         PARAM_EVENTID : [NSNumber numberWithInt:_event_id],
                         PARAM_ID : [NSNumber numberWithInt:_user._idx],
                         PARAM_TITLE : [txfTitle.text encodeString:NSUTF8StringEncoding],
                         PARAM_STARTTIME : [SelStartTime encodeString:NSUTF8StringEncoding],
                         PARAM_ENDTIME : [SelEndTime encodeString:NSUTF8StringEncoding],
                         PARAM_LOCATION : [txfLocation.text encodeString:NSUTF8StringEncoding],
                         PARAM_DETAIL : [txvDetail.text encodeString:NSUTF8StringEncoding]
                         };


AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
manager.requestSerializer = [AFHTTPRequestSerializer serializer];
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
[manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];

[manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    
    NSLog(@"%@", responseObject);
    
    int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
    
    if(nResultCode == CODE_SUCCESS) {
        
        _newEvent = [[EventEntity alloc] init];
        
        _newEvent._idx = [[responseObject valueForKey:RES_ID] intValue];
        _newEvent._title = txfTitle.text;
        _newEvent._startTime = SelStartTime;
        _newEvent._endTime = SelEndTime;
        _newEvent._location = txfLocation.text;
        _newEvent._detail = txvDetail.text;
        _newEvent._organiser = _user._firstname;
        _newEvent._status = 1;
        
        if (photoPath.length != 0) {
            
            [self addNewEventPhoto];
        }
        
        else {
            
            [APPDELEGATE hideLoadingView];
            
            [[JLToast makeText:NEW_EVENT_CREATED duration:2] show];
            
            [self gotoMyEventList];
            
        }
        
    } else if (nResultCode == START_TIME_ERROR) {
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:EVENT_TIME_ERROR positive:ALERT_OK negative:nil sender:self];
        
    } else if (nResultCode == END_TIME_ERROR) {
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:EVENT_TIME_ERROR positive:ALERT_OK negative:nil sender:self];
        
    } else if (nResultCode == START_END_TIME_ERROR){
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:EVENT_TIME_ERROR positive:ALERT_OK negative:nil sender:self];
        
    } else {
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:PHOTO_UPLOAD_FAIL positive:ALERT_OK negative:nil sender:self];
    }
    
    
} failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    
    NSLog(@"error: %@", error);
    
    [APPDELEGATE hideLoadingView];
    
    [APPDELEGATE showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
    
}];


}

- (void) addNewEventPhoto {
    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_ADD_NEWEVENTPHOTO];
    
    NSDictionary *params = @{
                             PARAM_ID : [NSNumber numberWithInt:_newEvent._idx]
                             };
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:[NSURL fileURLWithPath:photoPath] name:@"file" fileName:@"filename.jpg" mimeType:@"image/jpg" error:nil];
    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:nil
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      
                      if (error) {
                          
                          NSLog(@"Error: %@", error);
                          
                          [APPDELEGATE hideLoadingView];
                          
                          [APPDELEGATE showAlertDialog:nil message:PHOTO_UPLOAD_FAIL positive:ALERT_OK negative:nil sender:self];
                          
                      } else {
                          
                          [APPDELEGATE hideLoadingView];
                          
                          NSLog(@"%@ %@", response, responseObject);
                          
                          int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
                          
                          if(nResultCode == CODE_SUCCESS) {
                              
                              _newEvent._photoUrl = [responseObject valueForKey:RES_PHOTOURL];
                              
                              [[JLToast makeText:NEW_EVENT_CREATED duration:2] show];
                              
                              //[self gotoMyEventDetail];
                              
                              [self gotoMyEventList];
                              
                              
                          } else {
                              
                              [APPDELEGATE showAlertDialog:nil message:PHOTO_UPLOAD_FAIL positive:ALERT_OK negative:nil sender:self];
                          }
                      }
                  }];
    
    [uploadTask resume];
}

**********************/


#pragma mark - text field delegate

- (void) textFieldDidBeginEditing:(UITextField *)textField {
    
    if (textField == txfEmail) {
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3];
        
        CGAffineTransform transform = CGAffineTransformMakeTranslation(0.0, -20);
        [self.view setTransform:transform];
        
        [UIView commitAnimations];
        
    } else if (textField == txfPassword) {
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3];
        
        CGAffineTransform transform = CGAffineTransformMakeTranslation(0.0, -30);
        [self.view setTransform:transform];
        
        [UIView commitAnimations];
        
    }
    
}

- (void) textFieldDidEndEditing:(UITextField *)textField {
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    CGAffineTransform transform = CGAffineTransformMakeTranslation(0.0, 0.0);
    [self.view setTransform:transform];
    
    [UIView commitAnimations];
    
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    if(textField == txfEmail) {
        
        [txfPassword becomeFirstResponder];
        
    }
    
    [textField resignFirstResponder];
    
    return YES;
}


- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}


@end
