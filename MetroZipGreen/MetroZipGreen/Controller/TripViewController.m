//
//  TripViewController.m
//  MetroZipGreen
//
//  Created by Astolfo Arcuri on 17/07/16.
//  Copyright © 2016 Beauti. All rights reserved.
//

#import "TripViewController.h"
#import "Const.h"
#import "CarbonKit.h"

@interface TripViewController () <CarbonTabSwipeNavigationDelegate> {
    
    
    NSArray *items;
    
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
}

@end

@implementation TripViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initValue];
    
    [self initView];
}

- (void) initValue {
    
    items = @[ALL, UPCOMING, COMPLETED];
    
    carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:items delegate:self];
    [carbonTabSwipeNavigation insertIntoRootViewController:self];


}

- (void) initView {
    
    // set sos image at the top right of title
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sos"]]];
    self.navigationItem.rightBarButtonItem = rightItem;
  
    // carbon kit init
    
    UIColor *color = [UIColor colorWithRed:117.0 / 255 green:209.0 / 255 blue:0.0 / 255 alpha:1];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    carbonTabSwipeNavigation.toolbarHeight.constant = 50;
    carbonTabSwipeNavigation.toolbar.translucent = NO;
    carbonTabSwipeNavigation.toolbar.barTintColor = [UIColor colorWithRed:39.0/255. green:66.0/225.0 blue:0 alpha:1.0];
    [carbonTabSwipeNavigation setIndicatorColor:color];
    [carbonTabSwipeNavigation setTabExtraWidth:30];
    
    [carbonTabSwipeNavigation.carbonSegmentedControl setIndicatorHeight:5];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:self.view.frame.size.width/3.0f forSegmentAtIndex:0];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:self.view.frame.size.width/3.0f forSegmentAtIndex:1];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:self.view.frame.size.width/3.0f forSegmentAtIndex:2];
    
    // Custimize segmented control
    [carbonTabSwipeNavigation setNormalColor:[color colorWithAlphaComponent:1.0]
                                        font:[UIFont boldSystemFontOfSize:14]];
    [carbonTabSwipeNavigation setSelectedColor:[UIColor whiteColor] font:[UIFont boldSystemFontOfSize:14]];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - CarbonTabSwipeNavigation Delegate
// required
- (nonnull UIViewController *)carbonTabSwipeNavigation:
(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation
                                 viewControllerAtIndex:(NSUInteger)index {
    switch (index) {
            
        case 0:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"AllTripViewController"];
            
        case 1:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"UpcomingTripViewController"];
            
        default:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"CompletedTripViewController"];
    }
}

// optional
- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                 willMoveAtIndex:(NSUInteger)index {

}

- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                  didMoveAtIndex:(NSUInteger)index {

}

- (UIBarPosition)barPositionForCarbonTabSwipeNavigation:
(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation {
    return UIBarPositionTop; // default UIBarPositionTop
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
