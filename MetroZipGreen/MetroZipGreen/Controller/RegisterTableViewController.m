//
//  RegisterTableViewController.m
//  MetroZipGreen
//
//  Created by Astolfo Arcuri on 16/07/16.
//  Copyright © 2016 Beauti. All rights reserved.
//

#import "RegisterTableViewController.h"
#import "M13Checkbox.h"
#import "UITextView+Placeholder.h"

@interface RegisterTableViewController ()

@property (nonatomic, strong) M13Checkbox *chkTermsCondition;

@end

@implementation RegisterTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initView];
    
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initView {
    
    self.navigationItem.hidesBackButton = YES;
    
    // text field place holder
    self.txvAddress.text = @"";
    self.txvAddress.placeholder = @"Address";
    
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sos"]]];
    
    self.navigationItem.rightBarButtonItem = rightItem;
    
    //I agree to the
    _chkTermsCondition = [[M13Checkbox alloc] initWithTitle:@"I agree to the Terms and Conditions"];
    [_chkTermsCondition setCheckAlignment:M13CheckboxAlignmentLeft];
    _chkTermsCondition.strokeColor = [UIColor grayColor];
    _chkTermsCondition.checkColor = [UIColor grayColor];
    _chkTermsCondition.titleLabel.textColor = [UIColor grayColor];
    [_chkTermsCondition.titleLabel setFont:[UIFont systemFontOfSize:14]];
    _chkTermsCondition.tintColor = [UIColor clearColor];
    _chkTermsCondition.frame = CGRectMake(0, 8, _chkTermsCondition.frame.size.width, _chkTermsCondition.frame.size.height);
    [_chkTermsCondition addTarget:self action:@selector(checkChangedValue:) forControlEvents:UIControlEventValueChanged];
    
    [self.vForTermCondition addSubview:_chkTermsCondition];

}

- (void)checkChangedValue:(id)sender
{
    //NSLog(@"Changed Value");
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 20;
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}



/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}

@end
