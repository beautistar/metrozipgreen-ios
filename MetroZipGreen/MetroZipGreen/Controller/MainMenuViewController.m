//
//  MainMenuViewController.m
//  MetroZipGreen
//
//  Created by Astolfo Arcuri on 16/07/16.
//  Copyright © 2016 Beauti. All rights reserved.
//

#import "MainMenuViewController.h"

@interface MainMenuViewController ()

@end

@implementation MainMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initView];
}

- (void) initView {
    
    self.navigationItem.hidesBackButton = YES;
    
    
    // set logo image at the top center of title.
    UIImage *img = [UIImage imageNamed:@"title"];
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:img];
    
    // set sos image at the top right of title
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sos"]]];
    
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    self.navigationController.navigationBar.hidden = NO;
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


@end
