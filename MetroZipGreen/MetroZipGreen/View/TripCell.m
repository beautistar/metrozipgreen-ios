//
//  TripCell.m
//  MetroZipGreen
//
//  Created by Astolfo Arcuri on 17/07/16.
//  Copyright © 2016 Beauti. All rights reserved.
//

#import "TripCell.h"

@implementation TripCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setItem {
    
    self.vBg.layer.borderColor = [[UIColor colorWithRed:179/255.0 green:179/255.0 blue:179/255.0 alpha:0.7] CGColor];
}

@end
