//
//  ReqConst.h
//  MummyTrain
//
//  Created by Astolfo Arcuri on 03/06/16.
//  Copyright © 2016 Astolfo Arcuri. All rights reserved.
//

#ifndef ReqConst_h
#define ReqConst_h

/**
 **     Server URL Macro
 **/
#pragma mark -
#pragma mark - Server URL

#define SERVER_URL                          @"http:www.falconavl.com/hiatest/API/webapi.ashx/"

#define REQ_LOGIN                           @"login"
#define REQ_REGISTER                        @"register"
#define REQ_UPLOAD_PHOTO                    @"uploadPhoto"
//
#define REQ_ADD_NEWEVENT                    @"addNewEvent"
#define REQ_ADD_NEWEVENTPHOTO               @"addNewEventPhoto"
#define REQ_GET_ALLEVENTS                   @"getAllEvents"
#define REQ_GET_MYEVENTS                    @"getMyEvent"
#define REQ_SET_ATTEND                      @"setAttend"
#define REQ_GET_AUTHCODE                    @"getAuthCode"
#define REQ_UPDATEUSER                      @"updateUser"
#define REQ_GET_USERINFO                    @"getUsersInfo"
#define REQ_CANCEL_EVENT                    @"cancelEvent"
#define REQ_SETCOMMENT                      @"setComment"
#define REQ_GETCOMMENT                      @"getComment"

//
////Request params
//

#define PARAM_INTERFACE                     @"interface"
#define PARAM_METHOD                        @"method"
#define PARAM_PARAMETERS                    @"parameters"

//login
#define PARAM_USERNAME                      @"username"
#define PARAM_PASSWORD                      @"password"
#define PARAM_APPNAME                       @"appname"


// register
#define PARAM_USR                           @"usr"
#define PARAM_PWD                           @"pwd"
#define PARAM_COMPANYID                     @"companyid"
#define PARAM_NAME                          @"name"
#define PARAM_ADDRESS                       @"address"
#define PARAM_PINCODE                       @"pincode"
#define PARAM_PHONENO                       @"phoneno"
#define PARAM_EMPLOYEEID                    @"employeeid"
#define PARAM_GENDER                        @"gender"
#define PARAM_STARTPOINT                    @"startpoint"
#define PARAM_ENDPOINT                      @"endpoint"
#define PARAM_IMAGENAME                     @"imagename"
#define PARAM_CURRENTTRANSPORT              @"currenttransport"

/**
 **     Response Params
 **/
#pragma mark -
#pragma mark - Response Params

#define RES_SUCCESSFUL                      @"Successful"
#define RES_VALUE                           @"Value"

#define RES_DISPLAY                         @"display"
#define RES_VALUE_                          @"value"


#define RES_USERINFO                        @"user_info"

#define RES_ID                              @"id"
#define RES_USERINFOS                       @"user_infos"
#define RES_SURNAME                         @"surname"
#define RES_FIRSTNAME                       @"firstname"
#define RES_EMAIL                           @"email"


////
#define RES_EVENTINFOS                      @"event_infos"
#define RES_TITLE                           @"title"
#define RES_DATE                            @"date"
#define RES_STAETTIME                       @"start_time"
#define RES_ENDTIME                         @"end_time"
#define RES_DETAIL                          @"detail"
#define RES_ORGANISER                       @"organiser"
#define RES_STATUS                          @"status"
#define RES_LOCATION                        @"location"
#define RES_PHOTOURL                        @"photo_url"
#define RES_USERID                          @"user_id"
#define RES_CANCELLED                       @"cancelled"

#define RES_COMMENT_INFOS                   @"comment_infos"
#define RES_COMMENT                         @"comment"

/**
 **     Response Code
 **/

//#pragma mark -
//#pragma mark - Response Code
//
#define CODE_SUCCESS                        0
#define CODE_EXISTUSERNAME                  101
#define CODE_EXISTEMAIL                     102
#define CODE_UNREGISTER                     105
#define CODE_UNREGISTEREMAIL                108
#define CODE_WRONGPWD                       109
#define START_TIME_ERROR                    106
#define END_TIME_ERROR                      110
#define START_END_TIME_ERROR                111




#endif /* ReqConst_h */
