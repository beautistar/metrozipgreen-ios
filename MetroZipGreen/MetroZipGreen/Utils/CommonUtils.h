//
//  CommonUtils.h
//  MetroZipGreen
//
//  Created by Astolfo Arcuri on 20/07/16.
//  Copyright © 2016 Beauti. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Const.h"
#import "AppDelegate.h"
#import "UserDefault.h"
#import "NSString+Encode.h"
#import "UserEntity.h"

@interface CommonUtils : NSObject

@end
