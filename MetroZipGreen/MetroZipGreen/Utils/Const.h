//
//  Const.h
//  MetroZipGreen
//
//  Created by Astolfo Arcuri on 17/07/16.
//  Copyright © 2016 Beauti. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JLToast.h"
@import AFNetworking;
@import JLToast;
#import "ReqConst.h"
#import "PrefConst.h"
@interface Const : NSObject

/**
 **     Userdefaults and AppDelegate Macro
 **/
#pragma mark -
#pragma mark - UserDefaults and AppDelegate

// -------------------------------------------------------------
// string define
// -------------------------------------------------------------

// alert common
#define ALERT_TITLE                 @"MetroZipGreen"
#define   ALERT_OK                  @"OK"
#define   ALERT_CANCEL              @"CANCEL"

#define USERDEFAULTS [NSUserDefaults standardUserDefaults]
#define APPDELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])

/**
 ** number define
 **/


#define PROFILE_IMG_SIZE                256
#define SAVE_ROOT_PATH                  @"MetroZipGreen"

/**
 **
 **  string define
 **/

#define INPUT_FIRSTNAME             @"Please input your firstname."
#define INPUT_SURNAMR               @"Please input your surnamename."
#define INPUT_EMAIL                 @"Please input your email address."
#define INPUT_CORRECT_EMAIL         @"Please input your email address correctly."
#define INPUT_PWD                   @"Please input your password."
#define SELECT_PHOTO                @"Please select your photo."

//menu strings

#define ALL                             @"All"
#define UPCOMING                        @"Upcoming"
#define COMPLETED                       @"Completed"

#define RGBACOLOR(r,g,b,a) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]
#define RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(1.f)]

//--------------------------------------------------------------------------------------------------------

#define INPUT_FIRSTNAME             @"Please input your firstname."
#define INPUT_SURNAMR               @"Please input your surnamename."
#define INPUT_EMAIL                 @"Please input your email address."
#define INPUT_CORRECT_EMAIL         @"Please input your email address correctly."
#define INPUT_PWD                   @"Please input your password."
#define SELECT_PHOTO                @"Please select your photo."

#define EXIST_USERNAME              @"User name already exist."
#define EXIST_EMAIL                 @"Email already exist."
#define REGISTER_SUCCESS            @"Register success!"
#define PHOTO_UPLOAD_FAIL           @"Photo upload failed."
#define EVENT_TIME_ERROR            @"Selected event time was passed. \n please select the time again."
#define UPDATE_SUCCESS              @"Update success!"

#define INPUT_EVENTTITLE            @"Please input the event title."
#define SELECT_DATE                 @"Please select the event date"
#define SELECT_STARTTIME            @"Please select the event start time."
#define SELECT_ENDTIME              @"Please select the end time."
#define SELECT_LOCATION             @"Please select the location."
#define INPUT_DESCRIPTION           @"Please input about the event in detail."
#define WRITE_SONETHING             @"Please write something."

#define NEW_EVENT_CREATED           @"New event was created."
//
#define SEL_ATTEND                  @"Please select the attend status."

#define ATTEND_SUCCESS              @"Attending event was requested successfully."

#define UNREGISTERED                @"Email doesn't registered."
#define WRONG_PASSWORD              @"Wrong password"


#define CONN_ERROR                  @"Connecting to the server failed.\nPlease try again."

#define RES_ALREADYATTEND           @"You are already attending this event"

#define CONF_CANCEL                 @"Are you sure want to cancel this event?"

@end
